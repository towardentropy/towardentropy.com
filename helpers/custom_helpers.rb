module CustomHelpers
  def new_page_url(base_url, relative_path)
    # NOTE: there is no slash between "master" and the path since the url function always has a leading slash
    "#{base_url}/-/new/master/source#{relative_path}"
  end

  def edit_page_url(base_url, relative_path)
    "#{base_url}/-/sse/#{encode_path(relative_path)}/"
  end

  def encode_path(relative_path)
    ERB::Util.url_encode("master/source/#{relative_path}")
  end
end
