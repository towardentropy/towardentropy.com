# Toward Entropy

A catalog of humanity's march toward entropy.

## Society

* [Can History Predict the Future?](https://www.theatlantic.com/magazine/archive/2020/12/can-history-predict-future/616993/)
* [Welcome To The ‘Turbulent Twenties’](https://www.noemamag.com/welcome-to-the-turbulent-twenties/)
* [Collapsing Levels of Trust Are Devastating America](https://www.theatlantic.com/ideas/archive/2020/10/collapsing-levels-trust-are-devastating-america/616581/)
* [Can a collapse of global civilization be avoided?](http://rspb.royalsocietypublishing.org/content/280/1754/20122845)
* [Creeping Toward Crisis](https://www.nytimes.com/2017/04/06/opinion/creeping-toward-crisis.html)
* [In 1939, I didn’t hear war coming. Now its thundering approach can’t be ignored](https://www.theguardian.com/commentisfree/2017/aug/14/1939-second-world-war-fascist-thundering-approach-hitler)
* [Are we sleepwalking to World War III?](http://www.abc.net.au/news/2017-07-14/sleepwalking-to-world-war-three-stan-grant/8710390)

## Environment

* [Deep Adaptation (PDF)](http://www.lifeworth.com/deepadaptation.pdf): A Map for Navigating Climate Tragedy
* [The Climate Change Paper So Depressing It's Sending People to Therapy](https://www.vice.com/en/article/vbwpdb/the-climate-change-paper-so-depressing-its-sending-people-to-therapy)
* [The study on collapse they thought you should not read – yet – Professor Jem Bendell](https://jembendell.com/2018/07/26/the-study-on-collapse-they-thought-you-should-not-read-yet/)
